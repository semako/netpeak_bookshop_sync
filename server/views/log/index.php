<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'id_shop',
                'format' => 'raw',
                'value' => function ($model) {
                    $shop = \app\models\Shop::find()->byId($model->id_shop)->one();
                    return Html::a($shop->title, ['shop/view', 'id' => $model->id_shop]);
                }
            ],
            'dt',
            [
                'filter' => [
                    1 => 'Yes',
                    0 => 'No',
                ],
                'attribute' => 'success',
                'value' => function ($model) {
                    return $model->success ? 'Yes' : 'No';
                },
            ]
        ],
    ]); ?>

</div>
