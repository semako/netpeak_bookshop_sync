<?php

define('APP_ACCESS_KEY', 'abcdef123456');

return [
    'adminEmail' => 'admin@example.com',
    'client_point_version' => '/system/version/' . APP_ACCESS_KEY,
    'client_point_upsert' => '/book/upsert/' . APP_ACCESS_KEY . '/%s',
    'client_point_delete' => '/book/delete/' . APP_ACCESS_KEY . '/%s',
    'client_point_list' => '/book/get/' . APP_ACCESS_KEY,
];
