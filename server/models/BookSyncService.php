<?php

namespace app\models;

use \Exception as Exception;
use linslin\yii2\curl\Curl;
use \Yii as Yii;
use yii\helpers\Json;

class BookSyncService
{
    /**
     * @var Book
     */
    private $_owner;

    public function __construct(Book $owner)
    {
        $this->_owner = $owner;
    }

    public function upsert()
    {
        $shops = Shop::find()->all();

        foreach ($shops as $shop) {
            $event = new BookSyncEvent();
            $event->id_shop = $shop->id;
            $event->success = true;

            try {
                $response = Json::decode((new Curl())
                                ->setOption(CURLOPT_CONNECTTIMEOUT, 1)
                                ->setOption(CURLOPT_TIMEOUT, 3)
                                ->setOption(
                                    CURLOPT_POSTFIELDS,
                                    http_build_query([
                                        'description' => $this->_owner->description,
                                        'price' => $this->_owner->price,
                                        'amount' => $this->_owner->amount,
                                    ])
                                )
                                ->post(preg_replace('/[\/]+$/is', '', $shop->url) . sprintf(Yii::$app->params['client_point_upsert'], $this->_owner->id_isbn)));

                if (!$response || empty($response['success']) || !$response['success']) {
                    $event->success = false;
                }
            } catch (Exception $e) {
                $event->success = false;
            }

            // Trigger event
            $this->_owner->trigger(Shop::EVENT_AFTER_SYNC_COMPLETED, $event);
        }
    }

    public function delete()
    {
        $shops = Shop::find()->all();

        foreach ($shops as $shop) {
            $event = new BookSyncEvent();
            $event->id_shop = $shop->id;
            $event->success = true;

            try {
                $response = Json::decode((new Curl())
                    ->setOption(CURLOPT_CONNECTTIMEOUT, 1)
                    ->setOption(CURLOPT_TIMEOUT, 3)
                    ->post(preg_replace('/[\/]+$/is', '', $shop->url) . sprintf(Yii::$app->params['client_point_delete'], $this->_owner->id_isbn)));

                if (!$response || empty($response['success']) || !$response['success']) {
                    $event->success = false;
                }
            } catch (Exception $e) {
                $event->success = false;
            }

            // Trigger event
            $this->_owner->trigger(Shop::EVENT_AFTER_SYNC_COMPLETED, $event);
        }
    }
}