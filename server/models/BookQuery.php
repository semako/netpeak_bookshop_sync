<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Book]].
 *
 * @see Book
 */
class BookQuery extends \yii\db\ActiveQuery
{
    public function byIsbn($isbn)
    {
        $this->andWhere('[[id_isbn]] = :id_isbn', [':id_isbn' => $isbn]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return Book[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Book|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}