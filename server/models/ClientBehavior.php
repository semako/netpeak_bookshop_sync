<?php

namespace app\models;

use yii\base\Behavior;
use yii\base\Event;

class ClientBehavior extends Behavior
{
    public function events()
    {
        return [
            Shop::EVENT_SYNC_REQUEST => 'shopSyncRequest',
            Shop::EVENT_AFTER_SYNC_COMPLETED => 'afterShopSyncCompleted',
            Book::EVENT_AFTER_INSERT => 'afterBookInsert',
            Book::EVENT_AFTER_UPDATE => 'afterBookUpdate',
            Book::EVENT_AFTER_DELETE => 'afterBookDelete',
        ];
    }

    public function shopSyncRequest(Event $event)
    {
        $service = new ShopSyncService($this->owner);
        $service->syncAll();
    }

    public function afterShopSyncCompleted(ShopSyncEvent $event)
    {
        $log = new Log();
        $log->id_shop = intval($event->id_shop);
        $log->success = intval($event->success);
        $log->save(false);
    }

    public function afterBookSyncCompleted(BookSyncEvent $event)
    {
        $log = new Log();
        $log->id_shop = intval($event->id_shop);
        $log->success = intval($event->success);
        $log->save(false);
    }

    public function afterBookInsert($event)
    {
        $service = new BookSyncService($this->owner);
        $service->upsert();
    }

    public function afterBookUpdate($event)
    {
        $service = new BookSyncService($this->owner);
        $service->upsert();
    }

    public function afterBookDelete($event)
    {
        $service = new BookSyncService($this->owner);
        $service->delete();
    }
}
