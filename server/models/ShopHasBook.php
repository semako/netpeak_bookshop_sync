<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%shop_has_book}}".
 *
 * @property integer $id_shop
 * @property integer $id_book
 *
 * @property Book $idBook
 * @property Shop $idShop
 */
class ShopHasBook extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_has_book}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_shop', 'id_book'], 'required'],
            [['id_shop', 'id_book'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_shop' => 'Id Shop',
            'id_book' => 'Id Book',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdBook()
    {
        return $this->hasOne(Book::className(), ['id' => 'id_book']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdShop()
    {
        return $this->hasOne(Shop::className(), ['id' => 'id_shop']);
    }

    /**
     * @inheritdoc
     * @return ShopHasBookQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ShopHasBookQuery(get_called_class());
    }
}
