<?php

namespace app\models;

use \Exception as Exception;
use linslin\yii2\curl\Curl;
use \Yii as Yii;
use yii\helpers\Json;

class ShopSyncService
{
    /**
     * @var Shop
     */
    private $_owner;

    public function __construct(Shop $owner)
    {
        $this->_owner = $owner;
    }

    public function syncAll()
    {
        $shops = Shop::find()->all();

        foreach ($shops as $shop) {
            $this->syncOne($shop);
        }
    }

    public function syncOne(Shop $shop)
    {
        $loaded   = true;
        $success  = false;

        try {
            $response = Json::decode((new Curl())
                ->setOption(CURLOPT_CONNECTTIMEOUT, 1)
                ->setOption(CURLOPT_TIMEOUT, 3)
                ->get(preg_replace('/[\/]+$/is', '', $shop->url) . Yii::$app->params['client_point_list']));

            if (!$response || empty($response['success']) || !$response['success']) {
                $loaded = false;
            }
        } catch (Exception $e) {
            $response = false;
            $loaded   = false;
        }

        if ($loaded) {
            $success = true;

            foreach ($response['books'] as $item) {
                $book = Book::find()->byIsbn($item['id_isbn'])->one();

                if (is_null($book)) {
                    $book = new Book();
                }

                $transaction = Book::getDb()->beginTransaction();
                try {
                    $book->id_isbn = $item['id_isbn'];
                    $book->description = $item['description'];
                    $book->price = $item['price'];
                    $book->amount = $item['amount'];
                    $book->save(false);

                    $count = Book::getDb()->createCommand('SELECT COUNT([[id_shop]]) FROM {{%shop_has_book}} WHERE [[id_book]] = :id_book AND [[id_shop]] = :id_shop', [
                        ':id_book' => $book->id,
                        ':id_shop' => $shop->id,
                    ])->queryScalar();

                    if (!$count) {
                        $relation = new ShopHasBook();
                        $relation->id_shop = $shop->id;
                        $relation->id_book = $book->id;
                        $relation->save(false);
                    }

                    $transaction->commit();
                } catch(Exception $e) {
                    $transaction->rollBack();
                    $success = false;
                    break;
                }
            }
        }

        $event = new ShopSyncEvent();
        $event->id_shop = $shop->id;
        $event->success = $success;

        // Trigger event
        $this->_owner->trigger(Shop::EVENT_AFTER_SYNC_COMPLETED, $event);
    }
}