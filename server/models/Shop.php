<?php

namespace app\models;

use Yii;
use \linslin\yii2\curl\Curl as Curl;
use \Exception as Exception;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%shop}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $url
 *
 * @property Log[] $logs
 * @property ShopHasBook[] $shopHasBooks
 * @property Book[] $idBooks
 */
class Shop extends \yii\db\ActiveRecord
{
    const EVENT_SYNC_REQUEST = 'syncRequest';
    const EVENT_AFTER_SYNC_COMPLETED = 'afterSyncCompleted';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'url'], 'required'],
            [['title'], 'string', 'max' => 64],
            [['url'], 'string', 'max' => 255],
            [['url'], 'url'],
            [['url'], 'unique'],
            [['url'], 'validateUrl'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'url' => 'Url',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogs()
    {
        return $this->hasMany(Log::className(), ['id_shop' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopHasBooks()
    {
        return $this->hasMany(ShopHasBook::className(), ['id_shop' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdBooks()
    {
        return $this->hasMany(Book::className(), ['id' => 'id_book'])->viaTable('{{%shop_has_book}}', ['id_shop' => 'id']);
    }

    /**
     * @inheritdoc
     * @return ShopQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ShopQuery(get_called_class());
    }

    /**
     * Validating that end-point exists
     * @param $attribute
     * @param $params
     */
    public function validateUrl($attribute, $params)
    {
        try {
            $response = Json::decode((new Curl())
                ->setOption(CURLOPT_CONNECTTIMEOUT, 1)
                ->setOption(CURLOPT_TIMEOUT, 3)
                ->get(preg_replace('/[\/]+$/is', '', $this->$attribute) . Yii::$app->params['client_point_version']
            ));

            if (!$response || empty($response['success']) || !$response['success']) {
                $this->addError($attribute, 'No Client soft in specified end-point.');
            }
        } catch (Exception $e) {
            $this->addError($attribute, $e->getMessage());
        }
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            ClientBehavior::className(),
        ];
    }

    /**
     *
     */
    public static function syncAll()
    {
        (new self())->trigger(self::EVENT_SYNC_REQUEST);
    }
}
