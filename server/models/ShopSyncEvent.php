<?php

namespace app\models;

use yii\base\Event;

class ShopSyncEvent extends Event
{
    public $id_shop;
    public $success;
}