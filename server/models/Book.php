<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%book}}".
 *
 * @property integer $id
 * @property string $id_isbn
 * @property string $description
 * @property string $price
 * @property integer $amount
 *
 * @property ShopHasBook[] $shopHasBooks
 * @property Shop[] $idShops
 */
class Book extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%book}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_isbn'], 'required'],
            [['description'], 'string'],
            [['price'], 'number'],
            [['amount'], 'integer'],
            [['id_isbn'], 'string', 'max' => 64],
            [['id_isbn'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_isbn' => 'Id Isbn',
            'description' => 'Description',
            'price' => 'Price',
            'amount' => 'Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopHasBooks()
    {
        return $this->hasMany(ShopHasBook::className(), ['id_book' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdShops()
    {
        return $this->hasMany(Shop::className(), ['id' => 'id_shop'])->viaTable('{{%shop_has_book}}', ['id_book' => 'id']);
    }

    /**
     * @inheritdoc
     * @return BookQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BookQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            ClientBehavior::className(),
        ];
    }
}
