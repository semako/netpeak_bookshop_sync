<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m150530_193748_init
 */
class m150530_193748_init extends Migration
{
    /**
     *
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->getDriverName() == 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop}}', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . '(64) NOT NULL',
            'url' => Schema::TYPE_STRING . '(255) NOT NULL',
        ], $tableOptions);

        if ($this->db->getDriverName() == 'mysql') {
            $this->alterColumn('{{%shop}}', 'id', Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL AUTO_INCREMENT');
        } else {
            $this->alterColumn('{{%shop}}', 'id', Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL');
        }

        $this->createTable('{{%book}}', [
            'id' => Schema::TYPE_PK,
            'id_isbn' => Schema::TYPE_STRING . '(64) NOT NULL',
            'description' => Schema::TYPE_TEXT,
            'price' => Schema::TYPE_DECIMAL . '(11,2) NOT NULL DEFAULT 0.00',
            'amount' => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
        ], $tableOptions);

        if ($this->db->getDriverName() == 'mysql') {
            $this->alterColumn('{{%book}}', 'id', Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL AUTO_INCREMENT');
        } else {
            $this->alterColumn('{{%book}}', 'id', Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL');
        }

        $this->createTable('{{%log}}', [
            'id' => Schema::TYPE_PK,
            'id_shop' => Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL',
            'dt' => Schema::TYPE_TIMESTAMP,
            'success' => Schema::TYPE_SMALLINT . '(1) NOT NULL DEFAULT 1',
        ], $tableOptions);

        if ($this->db->getDriverName() == 'mysql') {
            $this->alterColumn('{{%log}}', 'id', Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL AUTO_INCREMENT');
        } else {
            $this->alterColumn('{{%log}}', 'id', Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL');
        }

        $this->createTable('{{%shop_has_book}}', [
            'id_shop' => Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL',
            'id_book' => Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL',

            (
                $this->db->getDriverName() == 'mysql' ?
                    'PRIMARY KEY (`id_shop`, `id_book`), ' .
                    'INDEX `idx-id_book` (`id_book` ASC), ' .
                    'INDEX `idx-id_shop` (`id_shop` ASC), ' .
                    'CONSTRAINT `fk-shop_has_book-id_shop-shop-id` FOREIGN KEY (`id_shop`) REFERENCES `netpeak_server`.`shop` (`id`) ON DELETE CASCADE ON UPDATE CASCADE, ' .
                    'CONSTRAINT `fk-shop_has_book-id_book-book-id` FOREIGN KEY (`id_book`) REFERENCES `netpeak_server`.`book` (`id`) ON DELETE CASCADE ON UPDATE CASCADE' :

                    'PRIMARY KEY (`id_shop`, `id_book`), ' .
                    'INDEX `idx-id_book` (`id_book` ASC), ' .
                    'INDEX `idx-id_shop` (`id_shop` ASC)'
            )
        ], $tableOptions);

        $this->createIndex('idx-unique-id_isbn', '{{%book}}', 'id_isbn', true);
        $this->createIndex('idx-id_shop', '{{%log}}', 'id_shop');

        if ($this->db->getDriverName() == 'mysql') {
            $this->addForeignKey('fk-log-id_shop-shop-id', '{{%log}}', 'id_shop', '{{%shop}}', 'id', 'cascade', 'cascade');
        }
    }

    /**
     *
     */
    public function down()
    {
        if ($this->db->getDriverName() == 'mysql') {
            $this->dropForeignKey('fk-log-id_shop-shop-id', '{{%log}}');
            $this->dropForeignKey('fk-shop_has_book-id_shop-shop-id', '{{%shop_has_book}}');
            $this->dropForeignKey('fk-shop_has_book-id_book-book-id', '{{%shop_has_book}}');
        }

        $this->dropTable('{{%shop}}');
        $this->dropTable('{{%book}}');
        $this->dropTable('{{%log}}');
        $this->dropTable('{{%shop_has_book}}');
    }
}
