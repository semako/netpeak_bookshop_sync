$(window).ready(function () {
    $('#shop-sync').on('click', function () {
        var $btn = $(this).button('loading');

        $.ajax('/index.php?r=shop/sync')
            .done(function() {
                alert('Sync Complete!');
                $btn.button('reset');
            })
            .fail(function() {
                alert('Error in Sync!');
                $btn.button('reset');
            });
    });
});