<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/app/loader.php';

use \app\core\App;
use \Silex\Application as Silex;

$routes = require_once APP_CONFIG_ROUTES;

(new App(new Silex(), $routes))->run();