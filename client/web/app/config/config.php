<?php

define('APP_VERSION', '1.0.0');
define('APP_DIR_BASE', realpath(__DIR__ . '/../'));
define('APP_DIR_CONFIG', APP_DIR_BASE . '/config');
define('APP_DIR_MODELS', APP_DIR_BASE . '/models');
define('APP_CONFIG_DB', APP_DIR_CONFIG . '/db.ini');
define('APP_CONFIG_ROUTES', APP_DIR_CONFIG . '/routes.php');
