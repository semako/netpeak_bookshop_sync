<?php

return [
    '/system/version/{accessKey}' => [ // Get version of client
        'method' => 'get',
        'callable' => ['app\api\System', 'version'],
    ],
    '/book/get/{accessKey}' => [ // Get list of books on client
        'method' => 'get',
        'callable' => ['app\api\Book', 'get'],
    ],
    '/book/upsert/{accessKey}/{isbn}' => [ // Upsert book
        'method' => 'post',
        'callable' => ['app\api\Book', 'upsert'],
    ],
    '/book/delete/{accessKey}/{isbn}' => [ // Delete book
        'method' => 'post',
        'callable' => ['app\api\Book', 'delete'],
    ],
];
