<?php

namespace app\core;

use \Silex\Provider\DoctrineServiceProvider;
use \Exception as Exception;

/**
 * Class App
 * @package app\core
 */
class App
{
    /**
     * @var App
     */
    public static $app;

    /**
     * @var \Silex\Application
     */
    private $_silex;

    /**
     * @param \Silex\Application $silex
     * @param Array $routes
     */
    public function __construct(\Silex\Application $silex, Array $routes) {
        $this->_silex = $silex;

        $this->_registerErrorHandler();
        $this->_registerServiceDb();
        $this->_registerServiceCheckAccess();
        $this->_registerRoutes($routes);

        self::$app = $this;
    }

    /**
     * Register DB Service
     */
    private function _registerServiceDb()
    {
        $this->getSilex()->register(new DoctrineServiceProvider(), [
            'db.options' => [
                'driver'   => 'pdo_mysql',
                'host'     => APP_MYSQL_HOST,
                'dbname'   => APP_MYSQL_DBNAME,
                'user'     => APP_MYSQL_LOGIN,
                'password' => APP_MYSQL_PASSWORD,
                'charset'  => 'utf8',
            ],
        ]);
    }

    /**
     * Register Check Access Service
     */
    private function _registerServiceCheckAccess()
    {
        $this->getSilex()->register(new CheckAccessServiceProvider(), [
            'check_access.access_key' => APP_ACCESS_KEY,
        ]);
    }

    /**
     * @return \Silex\Application
     */
    public function getSilex()
    {
        return $this->_silex;
    }

    /**
     * @return \Doctrine\DBAL\Connection
     */
    public function getDb()
    {
        try {
            return $this->getSilex()['db'];
        } catch (\Exception $e) {
            echo 'ERROR: ' . $e->getMessage();
            exit;
        }
    }

    /**
     * @return CheckAccess
     */
    public function getCheckAccess()
    {
        try {
            return $this->getSilex()['check_access'];
        } catch (\Exception $e) {
            echo 'ERROR: ' . $e->getMessage();
            exit;
        }
    }

    /**
     * Registering Routes
     * @param Array $routes
     */
    private function _registerRoutes(Array $routes)
    {
        foreach ($routes as $route => $config) {
            $method = $config['method'];
            $this->getSilex()->$method($route, $config['callable']);
        }
    }

    /**
     * Error handler
     */
    private function _registerErrorHandler()
    {
        $this->getSilex()->error(function (Exception $e) {
            return App::$app->getSilex()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        });
    }

    /**
     * Run application
     */
    public function run()
    {
        $this->getSilex()->run();
    }
}
