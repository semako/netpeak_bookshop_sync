<?php

namespace app\core;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class CheckAccess
 * @package app\core
 */
class CheckAccess
{
    /**
     * @var
     */
    private $_accessKey;

    /**
     * @param $accessKey
     */
    public function __construct($accessKey)
    {
        $this->_setAccessKey($accessKey);
    }

    /**
     * @param $accessKey
     */
    public function checkKeyIsValid($accessKey)
    {
        if ($accessKey !== $this->getAccessKey()) {
            throw new AccessDeniedException('Access Denied!');
        }
    }

    /**
     * @return mixed
     */
    public function getAccessKey()
    {
        return $this->_accessKey;
    }

    /**
     * @param $accessKey
     */
    private function _setAccessKey($accessKey)
    {
        $this->_accessKey = $accessKey;
    }
}