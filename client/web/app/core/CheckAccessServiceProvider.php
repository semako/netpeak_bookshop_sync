<?php

namespace app\core;

use Silex\Application;
use Silex\ServiceProviderInterface;
use \InvalidArgumentException as InvalidArgumentException;

class CheckAccessServiceProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        $app['check_access'] = $app->share(function ($app) {
            if (!isset($app['check_access.access_key'])) {
                throw new InvalidArgumentException('Access Key required by Check Access Service not provided!');
            }

            return new CheckAccess($app['check_access.access_key']);
        });
    }

    public function boot(Application $app)
    {
    }
}