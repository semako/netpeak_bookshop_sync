<?php

namespace app\api;

use app\catalog\book\BookProvider;
use app\core\App;
use \Exception as Exception;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Book
 * @package app\api
 */
class Book
{
    /**
     * Receive list of books
     * @param $accessKey
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function get($accessKey)
    {
        self::_checkAccess($accessKey);

        $provider = self::_getProvider();

        try {
            $list  = $provider->listBooks();
            $books = [];

            foreach ($list as $item) {
                $books[] = $item->getAttributes();
            }

            return App::$app->getSilex()->json([
                'success' => true,
                'books'   => $books,
            ]);
        } catch (Exception $e) {
            return App::$app->getSilex()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Update or Insert New book
     * @param Request $request
     * @param $accessKey
     * @param $isbn
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function upsert(Request $request, $accessKey, $isbn)
    {
        self::_checkAccess($accessKey);

        $description = $request->get('description');
        $price       = doubleval($request->get('price'));
        $amount      = intval($request->get('amount'));
        $provider    = self::_getProvider();
        $book        = $provider->loadByIsbn($isbn);

        try {
            if (!$book) {
                // New book
                $book = $provider->initFromData($isbn, $description, $price, $amount);
                $provider->createBook($book);
            } else {
                // Update
                $book->setAmount($amount);
                $book->setPrice($price);
                $book->setDescription($description);
                $provider->updateBook($book);
            }

            return App::$app->getSilex()->json([
                'isbn'    => $book->getIsbn(),
                'success' => true,
            ]);
        } catch (Exception $e) {
            return App::$app->getSilex()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Delete book
     * @param Request $request
     * @param $accessKey
     * @param $isbn
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function delete(Request $request, $accessKey, $isbn)
    {
        self::_checkAccess($accessKey);

        $provider = self::_getProvider();
        $book     = $provider->loadByIsbn($isbn);

        if ($book) {
            try {
                $provider->deleteBook($book);
            } catch (Exception $e) {
                return App::$app->getSilex()->json([
                    'message' => $e->getMessage(),
                    'success' => true,
                ]);
            }
        }

        return App::$app->getSilex()->json([
            'isbn'    => $isbn,
            'success' => true,
        ]);
    }

    /**
     * @return BookProvider
     */
    private static function _getProvider()
    {
        return new BookProvider();
    }

    /**
     * @param $accessKey
     */
    private static function _checkAccess($accessKey)
    {
        App::$app->getCheckAccess()->checkKeyIsValid($accessKey);
    }
}