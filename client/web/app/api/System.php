<?php

namespace app\api;

use app\core\App;

/**
 * Class System
 * @package app\api
 */
class System
{
    /**
     * @param $accessKey
     * @return string
     */
    public function version($accessKey)
    {
        App::$app->getCheckAccess()->checkKeyIsValid($accessKey);

        return App::$app->getSilex()->json([
            'success' => true,
            'version' => APP_VERSION,
        ]);
    }
}
