<?php

require_once __DIR__ . '/config/config.php';

/**
 * Load DB config, parses it and stores in constants:
 * APP_MYSQL_HOST
 * APP_MYSQL_LOGIN
 * APP_MYSQL_PASSWORD
 * APP_MYSQL_DBNAME
 *
 * @param $configFile
 * @throws Exception
 */
function loadDbConfig($configFile) {
    if (!file_exists($configFile)) {
        throw new Exception('DB Config file not found!');
    }

    $config = parse_ini_file($configFile);

    foreach ($config as $key => $value) {
        define('APP_' . strtoupper($key), $value);
    }
}

try {
    loadDbConfig(APP_CONFIG_DB);
} catch (Exception $e) {
    echo $e->getMessage();
    exit;
}