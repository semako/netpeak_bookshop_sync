<?php

namespace app\catalog\book;

interface BookProviderInterface
{
    public function initFromData($isbn, $description, $price, $amount);
    public function loadByIsbn($isbn);
    public function updateBook(BookInterface $book);
    public function deleteBook(BookInterface $book);
}
