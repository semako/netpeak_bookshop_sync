<?php

namespace app\catalog\book;

use app\core\App;

/**
 * Class BookProvider
 * @package app\catalog\book
 */
class BookProvider implements BookProviderInterface
{
    /**
     *
     */
    public function __construct()
    {

    }

    /**
     * @param $isbn
     * @param $description
     * @param $price
     * @param $amount
     * @return Book
     */
    public function initFromData($isbn, $description, $price, $amount)
    {
        return new Book($isbn, $description, $price, $amount);
    }

    /**
     * @param $isbn
     * @return Book|bool
     * @throws \Doctrine\DBAL\DBALException
     */
    public function loadByIsbn($isbn)
    {
        $stmt = App::$app->getDb()->executeQuery('SELECT * FROM book WHERE id_isbn = ?', [
            strtolower($isbn),
        ]);

        if (!$book = $stmt->fetch()) {
            return false;
        }

        return new Book($book['id_isbn'], $book['description'], $book['price'], $book['amount']);
    }

    /**
     * @param BookInterface $book
     * @return int
     */
    public function createBook(BookInterface $book)
    {
        return App::$app->getDb()->insert('book', $book->getAttributes());
    }

    /**
     * @param BookInterface $book
     * @return int
     */
    public function updateBook(BookInterface $book)
    {
        return App::$app->getDb()->update('book', $book->getAttributes(), [
            'id_isbn' => $book->getIsbn(),
        ]);
    }

    /**
     * @param BookInterface $book
     * @return int
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function deleteBook(BookInterface $book)
    {
        return App::$app->getDb()->delete('book', [
            'id_isbn' => $book->getIsbn(),
        ]);
    }

    /**
     * @return Book[]
     * @throws \Doctrine\DBAL\DBALException
     */
    public function listBooks()
    {
        $stmt  = App::$app->getDb()->executeQuery('SELECT * FROM book');
        $list  = $stmt->fetchAll();
        $books = [];

        foreach ($list as $item) {
            $books[] = $this->initFromData($item['id_isbn'], $item['description'], $item['price'], $item['amount']);
        }

        return $books;
    }
}
