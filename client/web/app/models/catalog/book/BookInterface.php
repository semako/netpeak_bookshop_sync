<?php

namespace app\catalog\book;

interface BookInterface
{
    public function getIsbn();
    public function getDescription();
    public function getPrice();
    public function getAmount();
    public function getAttributes();
}
