<?php

namespace app\catalog\book;

/**
 * Class Book
 * @package app\catalog\book
 */
class Book implements BookInterface
{
    /**
     * @var
     */
    private $_isbn;
    /**
     * @var
     */
    private $_description;
    /**
     * @var
     */
    private $_price;
    /**
     * @var
     */
    private $_amount;

    /**
     * @param $isbn
     * @param $description
     * @param $price
     * @param $amount
     */
    public function __construct($isbn, $description, $price, $amount)
    {
        if (empty($isbn)) {
            throw new \InvalidArgumentException('ISBN cannot be empty');
        }

        if (empty($price)) {
            throw new \InvalidArgumentException('Price cannot be empty');
        }

        $this->_isbn = $isbn;
        $this->_description = $description;
        $this->_price = $price;
        $this->_amount = $amount;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getIsbn() . ': ' . $this->getDescription();
    }

    /**
     * @return mixed
     */
    public function getIsbn()
    {
        return $this->_isbn;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->_price;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->_amount;
    }

    /**
     * @param mixed $isbn
     */
    public function setIsbn($isbn)
    {
        $this->_isbn = $isbn;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->_description = $description;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->_price = $price;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->_amount = $amount;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return [
            'id_isbn' => $this->getIsbn(),
            'description' => $this->getDescription(),
            'price' => $this->getPrice(),
            'amount' => $this->getAmount(),
        ];
    }
}
